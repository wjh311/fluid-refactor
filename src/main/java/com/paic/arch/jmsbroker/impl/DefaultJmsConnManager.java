package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.intf.JmsConnConverter;
import org.springframework.util.Assert;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class DefaultJmsConnManager implements JmsConnConverter {

    private ConnectionFactory connectionFactory;
    private final static boolean DEFAULT_SESSION_TRANSACTED = false;
    private final static int DEFAULT_SESSION_ACKNOWDLEDGEMODE = Session.AUTO_ACKNOWLEDGE;

    public DefaultJmsConnManager(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public Connection getConnection() throws JMSException {
        return getConnection(true);
    }

    private Connection getConnection(boolean isStartConn) throws JMSException {
        Assert.notNull(connectionFactory, "cannot get jms connection, connectionFactory should not be null");
        Connection connection = connectionFactory.createConnection();
        if (isStartConn) {
            startConnection(connection);
        }
        return connection;
    }

    @Override
    public void startConnection(Connection connection) throws JMSException {
        Assert.notNull(connection, "cannot start jms connection, connection is null");
        connection.start();
    }

    @Override
    public void stopConnection(Connection connection) throws JMSException {
        if (connection != null) {
            connection.stop();
        }
    }

    @Override
    public void closeConnection(Connection connection) throws JMSException {
        if (connection != null) {
            connection.close();
        }
    }

    @Override
    public Session getSession(Connection connection) throws JMSException {
        return getSession(connection, DEFAULT_SESSION_TRANSACTED, DEFAULT_SESSION_ACKNOWDLEDGEMODE);
    }

    @Override
    public Session getSession(Connection connection, boolean sessionTransacted, int sessionAcknowledgeMode) throws JMSException {
        Assert.notNull(connection, "cannot get jms session, connection is null");
        return connection.createSession(sessionTransacted, sessionAcknowledgeMode);
    }

    @Override
    public void closeSession(Session session) throws JMSException {
        if (session!=null) {
            session.close();
        }
    }
}
