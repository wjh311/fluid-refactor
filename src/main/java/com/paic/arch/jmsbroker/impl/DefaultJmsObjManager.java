package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.intf.JmsConnConverter;
import com.paic.arch.jmsbroker.intf.JmsObjConverter;
import org.springframework.util.Assert;

import javax.jms.*;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class DefaultJmsObjManager implements JmsObjConverter {

    private JmsConnConverter jmsConnConverter;

    public DefaultJmsObjManager(JmsConnConverter jmsConnConverter) {
        Assert.notNull(jmsConnConverter, "create jmsConnConverter error, jmsConnConverter is null");
        this.jmsConnConverter = jmsConnConverter;
    }

    @Override
    public JmsConnConverter getJmsConnConverter() {
        return jmsConnConverter;
    }

    @Override
    public MessageProducer createProducer(Session session, Destination destination) throws JMSException {
        Assert.notNull(session, "cannot create jms producer, session is null");
        Assert.notNull(session, "cannot create jms producer, destination is null");
        return session.createProducer(destination);
    }

    @Override
    public void closeProducer(MessageProducer producer) throws JMSException {
        if (producer != null) {
            producer.close();
        }
    }

    @Override
    public MessageConsumer createConsumer(Session session, Destination destination) throws JMSException {
        Assert.notNull(session, "cannot create jms consumer, session is null");
        Assert.notNull(session, "cannot create jms consumer, destination is null");
        return session.createConsumer(destination);
    }

    @Override
    public void closeConsumer(MessageConsumer consumer) throws JMSException {
        if (consumer != null) {
            consumer.close();
        }
    }
}
