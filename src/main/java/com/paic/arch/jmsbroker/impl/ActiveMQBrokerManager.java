package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.intf.MQBrokerConverter;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.Destination;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class ActiveMQBrokerManager implements MQBrokerConverter{

    private String brokerUrl;
    private BrokerService brokerService;

    public ActiveMQBrokerManager(String brokerUrl) throws Exception {
        this.brokerUrl = brokerUrl;
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    @Override
    public void startBroker() throws Exception {
        brokerService.start();
    }

    @Override
    public void stopBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    @Override
    public long getBrokerDestinationCount(String destinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(destinationName)) {
                return destination.getDestinationStatistics().getMessages().getCount();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", destinationName, brokerUrl));
    }
}
