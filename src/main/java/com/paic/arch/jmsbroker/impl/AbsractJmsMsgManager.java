package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.intf.*;
import org.springframework.util.Assert;

import javax.jms.*;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public abstract class AbsractJmsMsgManager implements JmsMsgConverter {

    protected JmsObjConverter jmsObjConverter;
    protected JmsConnConverter jmsConnConverter;

    public AbsractJmsMsgManager(JmsObjConverter jmsObjConverter) {
        Assert.notNull(jmsObjConverter, "create JmsObjManager error, jmsConnConverter is null");
        this.jmsObjConverter = jmsObjConverter;
        this.jmsConnConverter = jmsObjConverter.getJmsConnConverter();
    }

    protected <T> T executeSend(final String destinationName, final ProducerCallback<T> callback) throws JMSException {
        Assert.notNull(callback, "send jms message error, callback should not be null");
        return execute(new SessionCallback<T>() {
            @Override
            public T doInJms(Session session) throws JMSException {
                Destination destination = resolveDestinationName(session, destinationName);
                Assert.notNull(destination, "send jms message error, destination should not be null");
                MessageProducer producer = null;
                try {
                    producer = jmsObjConverter.createProducer(session, destination);
                    return callback.doInJms(session, producer);
                } finally {
                    jmsObjConverter.closeProducer(producer);
                }
            }
        });
    }

    protected <T> T executeSend(final Destination destination, final ProducerCallback<T> callback) throws JMSException {
        Assert.notNull(callback, "send jms message error, callback should not be null");
        Assert.notNull(destination, "send jms message error, destination should not be null");
        return execute(new SessionCallback<T>() {
            @Override
            public T doInJms(Session session) throws JMSException {
                MessageProducer producer = null;
                try {
                    producer = jmsObjConverter.createProducer(session, destination);
                    return callback.doInJms(session, producer);
                } finally {
                    jmsObjConverter.closeProducer(producer);
                }
            }
        });
    }

    protected <T> T executeRecv(final String destinationName, final ConsumerCallback<T> callback) throws JMSException {
        Assert.notNull(callback, "recv jms message error, callback should not be null");
        return execute(new SessionCallback<T>() {
            @Override
            public T doInJms(Session session) throws JMSException {
                Destination destination = resolveDestinationName(session, destinationName);
                Assert.notNull(destination, "recv jms message error, destination should not be null");
                MessageConsumer consumer = null;
                try {
                    consumer = jmsObjConverter.createConsumer(session, destination);
                    return callback.doInJms(session, consumer);
                } finally {
                    jmsObjConverter.closeConsumer(consumer);
                }
            }
        });
    }

    protected <T> T executeRecv(final Destination destination, final ConsumerCallback<T> callback) throws JMSException {
        Assert.notNull(callback, "recv jms message error, callback should not be null");
        Assert.notNull(destination, "recv jms message error, destination should not be null");
        return execute(new SessionCallback<T>() {
            @Override
            public T doInJms(Session session) throws JMSException {
                MessageConsumer consumer = null;
                try {
                    consumer = jmsObjConverter.createConsumer(session, destination);
                    return callback.doInJms(session, consumer);
                } finally {
                    jmsObjConverter.closeConsumer(consumer);
                }
            }
        });
    }

    protected <T> T execute(final SessionCallback<T> callback) throws JMSException {
        Connection connection = null;
        Session session = null;
        try {
            connection = jmsConnConverter.getConnection();
            session = jmsConnConverter.getSession(connection);
            Assert.notNull(session, "cannot get any session");
            return callback.doInJms(session);
        } finally {
            jmsConnConverter.closeSession(session);
            jmsConnConverter.closeConnection(connection);
        }
    }

    protected Destination resolveDestinationName(Session session, String destinationName) throws JMSException {
        return session.createQueue(destinationName);
    }
}
