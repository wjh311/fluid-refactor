package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.intf.*;
import org.springframework.util.Assert;

import javax.jms.*;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class DefaultJmsMsgManager extends AbsractJmsMsgManager{

    public DefaultJmsMsgManager(JmsObjConverter jmsObjConverter) {
        super(jmsObjConverter);
    }

    @Override
    public void sendTextMsgByDestinationName(String destinationName, String messageStr) throws JMSException {
        executeSend(destinationName, new ProducerCallback<Object>() {
            @Override
            public Object doInJms(Session session, MessageProducer producer) throws JMSException {
                producer.send(session.createTextMessage(messageStr));
                return null;
            }
        });
    }

    @Override
    public void sendTextMsg(final Destination destination, final String messageStr) throws JMSException {
        executeSend(destination, new ProducerCallback<Object>() {
            @Override
            public Object doInJms(Session session, MessageProducer producer) throws JMSException {
                producer.send(session.createTextMessage(messageStr));
                return null;
            }
        });
    }

    @Override
    public void sendMsg(final Destination destination, ProducerCallback callback) throws JMSException {
        executeSend(destination, callback);
    }

    @Override
    public void sendMsg(final Destination destination, final Message message) throws JMSException {
        Assert.notNull(message, "send jms message error, message should not be null");
        executeSend(destination, new ProducerCallback<Object>() {
            @Override
            public Object doInJms(Session session, MessageProducer producer) throws JMSException {
                producer.send(message);
                return null;
            }
        });
    }

    @Override
    public void sendMsgByDestinationName(String destinationName, Message message) throws JMSException {
        Assert.notNull(message, "send jms message error, message should not be null");
        executeSend(destinationName, new ProducerCallback<Object>() {
            @Override
            public Object doInJms(Session session, MessageProducer producer) throws JMSException {
                producer.send(message);
                return null;
            }
        });
    }

    @Override
    public Message recvMsg(Destination destination, long timeout) throws JMSException {
        return executeRecv(destination, new ConsumerCallback<Message>() {
            @Override
            public Message doInJms(Session session, MessageConsumer consumer) throws JMSException {
                return timeout>=0 ? consumer.receive(timeout) : consumer.receive();
            }
        });
    }

    @Override
    public <T> T recvMsg(final Destination destination, ConsumerCallback<T> callback) throws JMSException {
        return executeRecv(destination, callback);
    }

    @Override
    public Message recvMsgByDestinationName(String destinationName, long timeout) throws JMSException {
        return executeRecv(destinationName, new ConsumerCallback<Message>() {
            @Override
            public Message doInJms(Session session, MessageConsumer consumer) throws JMSException {
                return timeout>=0 ? consumer.receive(timeout) : consumer.receive();
            }
        });
    }


}
