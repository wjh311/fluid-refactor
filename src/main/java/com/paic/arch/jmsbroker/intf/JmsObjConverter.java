package com.paic.arch.jmsbroker.intf;

import javax.jms.*;

/**
 * JMS对象管理器
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public interface JmsObjConverter {

    JmsConnConverter getJmsConnConverter();

    MessageProducer createProducer(Session session, Destination destination) throws JMSException;

    void closeProducer(MessageProducer producer) throws JMSException;

    MessageConsumer createConsumer(Session session, Destination destination) throws JMSException;

    void closeConsumer(MessageConsumer consumer) throws JMSException;
}
