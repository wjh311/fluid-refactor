package com.paic.arch.jmsbroker.intf;

/**
 * BROKER抽象接口
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public interface MQBrokerConverter {
    void startBroker() throws Exception;

    void stopBroker() throws Exception;

    long getBrokerDestinationCount(String destinationName) throws Exception;
}
