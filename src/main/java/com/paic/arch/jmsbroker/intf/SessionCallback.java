package com.paic.arch.jmsbroker.intf;

import javax.jms.JMSException;
import javax.jms.Session;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public interface SessionCallback<T> {
    T doInJms(Session session) throws JMSException;

}
