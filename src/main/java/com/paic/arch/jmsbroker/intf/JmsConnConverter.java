package com.paic.arch.jmsbroker.intf;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * JMS连接管理器
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public interface JmsConnConverter {

    Connection getConnection() throws JMSException;

    void startConnection(Connection connection) throws JMSException;

    void stopConnection(Connection connection) throws JMSException;

    void closeConnection(Connection connection) throws JMSException;

    Session getSession(Connection connection) throws JMSException;

    Session getSession(Connection connection, boolean sessionTransacted, int sessionAcknowledgeMode) throws JMSException;

    void closeSession(Session session) throws JMSException;
}
