package com.paic.arch.jmsbroker.intf;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

/**
 * JMS消息管理器
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public interface JmsMsgConverter {

    void sendMsg(final Destination destination, final ProducerCallback callback) throws JMSException;

    void sendMsg(final Destination destination, final Message message) throws JMSException;

    void sendMsgByDestinationName(final String destinationName, final Message message) throws JMSException;

    void sendTextMsg(final Destination destination, final String messageStr) throws JMSException;

    void sendTextMsgByDestinationName(final String destinationName, final String messageStr) throws JMSException;

    Message recvMsg(final Destination destination, final long timeout) throws JMSException;

    <T> T recvMsg(final Destination destination, ConsumerCallback<T> callback) throws JMSException;

    Message recvMsgByDestinationName(final String destinationName, final long timeout) throws JMSException;

}
