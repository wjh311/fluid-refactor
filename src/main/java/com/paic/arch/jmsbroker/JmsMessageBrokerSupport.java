package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.factory.JmsMsgConverterAccessor;
import com.paic.arch.jmsbroker.factory.MQBrokerConverterAccessor;
import com.paic.arch.jmsbroker.intf.JmsMsgConverter;
import com.paic.arch.jmsbroker.intf.MQBrokerConverter;
import org.slf4j.Logger;

import javax.jms.*;

import static org.slf4j.LoggerFactory.getLogger;

public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);

    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    private static final String DEFAULT_JMS_TYPE = "activemq";
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String brokerUrl;
    private String jmsType;

    private MQBrokerConverter mqBrokerConverter;
    private JmsMsgConverter jmsMsgConverter;

    /**
     * 实例对象赋值
     * @param aBrokerUrl broker地址
     * @param jmsType JMS消息管理器类型
     */
    private JmsMessageBrokerSupport(String aBrokerUrl, String jmsType) {
        this.brokerUrl = aBrokerUrl;
        this.jmsType = jmsType;
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + SocketFinder.findNextAvailablePortBetween(41616, 50000));
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBrokerSupport jmsSupport = bindToBrokerAtUrl(aBrokerUrl);
        jmsSupport.createEmbeddedBroker();
        jmsSupport.startEmbeddedBroker();
        return jmsSupport;
    }

    /**
     * 利用Broker管理对象剥离broker管理代码，采用工厂模式创建
     * @throws Exception
     */
    private void createEmbeddedBroker() throws Exception {
        mqBrokerConverter = MQBrokerConverterAccessor.createMQBrokerConverter(brokerUrl, jmsType);
    }

    private void startEmbeddedBroker() throws Exception {
        mqBrokerConverter.startBroker();
    }

    public void stopTheRunningBroker() throws Exception {
        mqBrokerConverter.stopBroker();
    }

    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return mqBrokerConverter.getBrokerDestinationCount(aDestinationName);
    }

    /**
     * 方法为向下兼容提供，采用默认的JMS类型
     * @param aBrokerUrl
     * @return
     * @throws Exception
     */
    @Deprecated
    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return new JmsMessageBrokerSupport(aBrokerUrl, DEFAULT_JMS_TYPE);
    }

    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl, String type) throws Exception {
        return new JmsMessageBrokerSupport(aBrokerUrl, type);
    }

    /**
     * 根据broker地址和JMS消息管理器类型创建JMS消息管理器
     */
    private synchronized void initJmsMsgConverter(){
        if (jmsMsgConverter==null) {
            synchronized (this) {
                if (jmsMsgConverter==null) {
                    // 外观模式包装生成JMS消息管理器
                    jmsMsgConverter = JmsMsgConverterAccessor.makeJmsMsgConverter(brokerUrl, jmsType);
                }
            }
        }
    }

    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        initJmsMsgConverter();
        try {
            jmsMsgConverter.sendTextMsgByDestinationName(aDestinationName, aMessageToSend);
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        initJmsMsgConverter();
        try {
            Message message = jmsMsgConverter.recvMsgByDestinationName(aDestinationName, aTimeout);
            if (message==null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            return ((TextMessage) message).getText();
        } catch (JMSException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public String getBrokerUrl() {
        return brokerUrl;
    }

    public String getJmsType() {
        return jmsType;
    }

    public void setJmsType(String jmsType) {
        this.jmsType = jmsType;
    }

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    public class NoMessageReceivedException extends RuntimeException {
        public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }
}
