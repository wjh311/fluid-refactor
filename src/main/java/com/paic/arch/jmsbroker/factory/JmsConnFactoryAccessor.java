package com.paic.arch.jmsbroker.factory;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.ConnectionFactory;

/**
 * JMS连接工厂
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class JmsConnFactoryAccessor {
    public static ConnectionFactory createConnectionFactory(String brokerUrl, String type){
        if ("activemq".equalsIgnoreCase(type)) {
            return new ActiveMQConnectionFactory(brokerUrl);
        }
        return null;
    }
}
