package com.paic.arch.jmsbroker.factory;

import com.paic.arch.jmsbroker.impl.ActiveMQBrokerManager;
import com.paic.arch.jmsbroker.intf.MQBrokerConverter;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.ConnectionFactory;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class MQBrokerConverterAccessor {

    public static MQBrokerConverter createMQBrokerConverter(String brokerUrl, String type) throws Exception {
        if ("activemq".equalsIgnoreCase(type)) {
            return new ActiveMQBrokerManager(brokerUrl);
        }
        return null;
    }
}
