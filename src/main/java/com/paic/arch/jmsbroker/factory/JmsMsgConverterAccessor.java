package com.paic.arch.jmsbroker.factory;

import com.paic.arch.jmsbroker.factory.JmsConnFactoryAccessor;
import com.paic.arch.jmsbroker.impl.DefaultJmsConnManager;
import com.paic.arch.jmsbroker.impl.DefaultJmsMsgManager;
import com.paic.arch.jmsbroker.impl.DefaultJmsObjManager;
import com.paic.arch.jmsbroker.intf.JmsConnConverter;
import com.paic.arch.jmsbroker.intf.JmsMsgConverter;
import com.paic.arch.jmsbroker.intf.JmsObjConverter;

import javax.jms.ConnectionFactory;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/28
 */

public class JmsMsgConverterAccessor {
    public static JmsMsgConverter makeJmsMsgConverter(String brokerUrl, String jmsMsgConverterType){
        // 采用工厂模式创建JMS连接工厂
        ConnectionFactory connectionFactory = JmsConnFactoryAccessor.createConnectionFactory(brokerUrl, jmsMsgConverterType);
        // 创建JMS连接管理器
        JmsConnConverter jmsConnConverter = new DefaultJmsConnManager(connectionFactory);
        // 创建JMS对象管理器
        JmsObjConverter jmsObjConverter = new DefaultJmsObjManager(jmsConnConverter);
        // 创建JMS消息管理器
        return new DefaultJmsMsgManager(jmsObjConverter);
    }
}
